<?php
/**
 * Template Name: Student listing
 * Template Post Type: post, page
 *
 * The template for displaying students
 *
 *
 * @package WordPress
 * @subpackage Adrenaline
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php

/* 
Create a student listing page that lists all 7 students and the courses for each student. URL: /students
Note:
§  The course listing needs to order by name
§  No need any css/javascript
 

            Name: <first_name> <last_name>

            Courses:

§  <faculty> - <course name>
§  …
 
            Name: Joel Lee

            Courses:

§  Art - Bachelor of Arts
§  Law - Bachelor of Laws
 
·         Create a pagination for the listing page and set the page size to 3. It can be the default WP pagination or custom pagination

	*/
	  
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		// args
		$args = array(
			'numberposts'	=> -1,
			'post_type'		=> 'student',
			'meta_key'		=> 'last_name',
			'orderby'		=> 'meta_value',
			'order'			=> 'ASC',
			'post_status'	=> 'publish',
			'posts_per_page' => 3,
  			'paged'          => $paged
		);

		$query = new WP_Query( $args );
		
		if ( $query->have_posts() ) {
			echo '<ul>';
			while ( $query->have_posts() ) : $query->the_post();
				$title = get_the_title();
				$url = get_the_permalink();
				$courses = get_field('course');
				
				echo '<li>';
				echo 'Name: <a href="'.$url.'">'.$title.'</a><br>';
					
				echo 'Courses:<br>';
				foreach( $courses as $post): 
        			setup_postdata($post);
        			$faculty = get_field('faculty');
        			$title = get_the_title();
        			echo $faculty.' - '.$title.'<br>';
   				endforeach;
				echo '</li>';					
			endwhile; // End of the loop.
			echo '</ul>';
			
			$big = 999999999;
			 echo paginate_links( array(
				'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $query->max_num_pages
			) );

			wp_reset_postdata();
		
		} ?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
